package com.example.nikitaarora.contactsapp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.List;

/**
 * Created by nikita.arora on 12/8/2015.
 */
public class ContactsAdapter extends BaseAdapter implements SectionIndexer {

    private Context mContext;
    private List<PhoneBook> phonebookList;

    public ContactsAdapter(Context context, List<PhoneBook> list) {
        this.mContext = context;
        this.phonebookList = list;
    }


    @Override
    public int getCount() {
        return phonebookList.size();
    }

    @Override
    public Object getItem(int position) {
        return phonebookList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PhoneBook entry = phonebookList.get(position);
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_item, null);
        }

        ImageView mAvatar = (ImageView) convertView.findViewById(R.id.avatar);
        TextView mName = (TextView) convertView.findViewById(R.id.name);
        TextView mNumber = (TextView) convertView.findViewById(R.id.number);

        mAvatar.setImageBitmap(entry.getAvatar());
        mName.setText(entry.getName());
        mNumber.setText(entry.getNumber());

        char alphabet = entry.getName().charAt(0);

        /*TextDrawable drawable = TextDrawable.builder()
                .buildRect(alphabet, Color.RED);

        ImageView image = (ImageView) findViewById(R.id.image_view);
        image.setImageDrawable(drawable);*/
        return convertView;
    }

    @Override
    public Object[] getSections() {
        return new Object[0];
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return 0;
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }
}

