package com.example.nikitaarora.contactsapp;

import android.graphics.Bitmap;

/**
 * Created by nikita.arora on 12/8/2015.
 */

public class PhoneBook {

    private Bitmap avatar;
    private String name, number;

    public void setAvatar(Bitmap avatar) {
        this.avatar = avatar;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return this.name;
    }

    public String getNumber() {
        return this.number;
    }

    public Bitmap getAvatar() {
        return this.avatar;
    }
}

